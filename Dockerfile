# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM omrumbakitemiz/node-frontend as build-stage
WORKDIR /app
COPY package.json /app/
COPY yarn.lock /app/
RUN yarn install
COPY ./ /app/
ARG configuration=production
# RUN yarn lint
# RUN yarn test-ci
RUN yarn build --output-path=./dist/out --configuration $configuration

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM twalter/openshift-nginx
COPY --from=build-stage /app/dist/out/ /usr/share/nginx/html
# Copy the default nginx.conf provided by omrumbakitemiz/node-frontend
COPY --from=build-stage /nginx.conf /etc/nginx/conf.d/default.conf
