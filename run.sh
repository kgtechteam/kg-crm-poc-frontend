#!/bin/bash

printf "Run script started... \n\n"
sleep 2;

IMAGE_NAME=$1
IMAGE_TAG=$2

HOST_PORT=$3
CONTAINER_PORT=$4

PREVIOUS_IMAGE_NAME=$(docker ps -a -q --filter ancestor="${IMAGE_NAME}":"${IMAGE_TAG}" --format="{{.ID}}")

docker rm "$(docker stop "${PREVIOUS_IMAGE_NAME}")"

docker run -p "${HOST_PORT}":"${CONTAINER_PORT}" -d "${IMAGE_NAME}":"${IMAGE_TAG}"

echo "Run script finished..."
