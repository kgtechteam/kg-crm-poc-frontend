export const environment = {
  production: true,
  environmentName: 'STAGING',
  baseApiUrl: 'http://prod.url.com',
};
