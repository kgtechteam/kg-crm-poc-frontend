import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from './main/main.component';
import {mainRoutes} from './main/main.routes';


const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [...mainRoutes]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
