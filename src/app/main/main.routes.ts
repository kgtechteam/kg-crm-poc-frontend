import {Routes} from '@angular/router';

/**
 * This file contains all sub modules and components.
 */

export const mainRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'customer'
  },
  {
    path: 'customer',
    loadChildren: () => import('../pages/customer/customer.module').then(m => m.CustomerModule)
  },
  {
    path: 'address',
    loadChildren: () => import('../pages/address/address.module').then(m => m.AddressModule)
  },
  {
    path: 'product',
    loadChildren: () => import('../pages/product/product.module').then(m => m.ProductModule)
  },
  {
    path: 'order',
    loadChildren: () => import('../pages/order/order.module').then(m => m.OrderModule)
  }
];
