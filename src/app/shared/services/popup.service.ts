import { Injectable } from '@angular/core';
import {NzModalService} from 'ng-zorro-antd';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PopupService {

  constructor(
    private modalService: NzModalService,
    private router: Router) { }

  openPopup(path: string) {
    this.modalService.success({
      nzTitle: 'Successful',
      nzContent: 'Registration Successful',
      nzOkText: 'Ok',
      nzOnOk: () => this.router.navigate([`${path}`])
    });
  }
}
