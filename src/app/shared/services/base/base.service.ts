import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export abstract class BaseService {

  baseApiUrl = environment.baseApiUrl;

  constructor(private httpClient: HttpClient) {
  }

  abstract getPath(): string;

  abstract getHttpParams(): HttpParams;


  protected getWithOptions<T>(url: string, options: any): Observable<HttpResponse<T>> {
    return this.httpClient.get<T>(this.getFullPath(url), {params: options, observe: 'response'});
  }

  protected getBlobRequest<T>(url: string, params?: HttpParams) {
    return this.httpClient.get(this.getFullPath(url), {
      headers: new HttpHeaders({'Content-Type': 'application/json'}),
      responseType: 'blob',
      params,
      observe: 'response'
    });
  }

  protected get<T>(url?: string): Observable<T> {
    if (this.getHttpParams()) {
      return this.httpClient.get<T>(this.getFullPath(url), {params: this.getHttpParams()});
    }
    return this.httpClient.get<T>(this.getFullPath(url));
  }

  protected post<T>(body?: any, url?: string, params?: HttpParams): Observable<T> {
    if (params) {
      return this.httpClient.post<T>(this.getFullPath(url), {params}, {params});
    } else {
      return this.httpClient.post<T>(this.getFullPath(url), body);
    }
  }

  protected put<T>(url: string, body?: any): Observable<T> {
    return this.httpClient.put<T>(this.getFullPath(url), body);
  }

  protected purePost<T>(url: string, body?: any): Observable<T> {
    const generatedUrl = `${this.baseApiUrl}/${url}`;
    return this.httpClient.post<T>(generatedUrl, body);
  }

  protected delete<T>(url?: string, params?: any, body?: any): Observable<HttpEvent<T>> {
    return this.httpClient.request<T>('delete', this.getFullPath(url), {params, observe: 'response', body});
  }

  private getFullPath(url: string) {
    if (url && url.length > 0) {
      return `${this.baseApiUrl}/${this.getPath()}/${url}`;
    } else {
      return `${this.baseApiUrl}/${this.getPath()}`;
    }
  }
}
