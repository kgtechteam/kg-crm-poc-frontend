import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SharedNgZorroModule} from './shared';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgxSoapModule} from 'ngx-soap';
import {LayoutModule} from './layout/layout.module';
import {RouterModule} from '@angular/router';
import {en_US, NZ_I18N} from 'ng-zorro-antd/i18n';
import en from '@angular/common/locales/en';
import {registerLocaleData} from '@angular/common';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgxSoapModule,
    LayoutModule,
    RouterModule,
    SharedNgZorroModule
  ],
  providers: [
    {provide: NZ_I18N, useValue: en_US},
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
