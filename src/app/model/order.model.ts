export class OrderModel {
  id: string;
  createDate: string;
  productId: number;
  status: string;
  systemMessage: string;
  tckn: 0;

}

export class PaginationModel<T> {
  content: T;
  pageable: Pageable;
  totalPages: number;
  last: boolean;
  totalElements: number;
  size: number;
  number: number;
  first: boolean;
  sort: Sort;
  numberOfElements: number;
}

interface Pageable {
  sort: Sort;
  offset: number;
  pageSize: number;
  pageNumber: number;
  unpaged: boolean;
  paged: boolean;
}

interface Sort {
  unsorted: boolean;
  sorted: boolean;
}
