export class ProductModel {
  currency: string;
  enable: boolean;
  id: number;
  name: string;
  price: number;
  stock: number;
}
