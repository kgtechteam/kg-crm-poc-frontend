import {AddressModel} from './address.model';

export class CustomerModel {
  name?: string;
  surname?: string;
  tckn?: number;
  birthYear?: number;
  mail?: string;
  address = new AddressModel();
}
