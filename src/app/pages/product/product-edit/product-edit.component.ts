import {Component, OnInit} from '@angular/core';
import {ProductService} from '../product.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NzNotificationService} from 'ng-zorro-antd';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {ProductModel} from '../../../model/product.model';
import {PopupService} from '../../../shared/services/popup.service';

@Component({
  selector: 'kg-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit {

  productModel = new ProductModel();
  productId: number;

  productFormGroup = this.formBuilder.group({
    name: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required),
    currency: new FormControl('', Validators.required),
    stock: new FormControl('', Validators.required),
    enabled: new FormControl(false)
  }, {updateOn: 'blur'});

  constructor(
    private productService: ProductService,
    private router: Router,
    private toastrService: NzNotificationService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private popupService: PopupService
  ) {
    this.productId = Number(this.route.snapshot.paramMap.get('id'));
  }

  ngOnInit() {
    this.productService.findByProduct(this.productId).subscribe(result => {
      this.productModel = result;
      this.fillingInProductInformationToBeEditedInTheForm();
    });
  }

  fillingInProductInformationToBeEditedInTheForm() {
    this.productFormGroup.patchValue({
      name: this.productModel.name,
      price: this.productModel.price,
      currency: this.productModel.currency,
      stock: this.productModel.stock,
      enabled: this.productModel.enable
    });
  }

  updateProduct() {
    this.productModel.id = this.productId;
    this.productModel.name = this.productFormGroup.get('name').value;
    this.productModel.price = Number(this.productFormGroup.get('price').value);
    this.productModel.currency = this.productFormGroup.get('currency').value;
    this.productModel.stock = Number(this.productFormGroup.get('stock').value);
    this.productModel.enable = this.productFormGroup.get('enabled').value;
    if (!this.productFormGroup.invalid) {
      this.productService.updateProduct(this.productModel).subscribe(() => {
        this.popupService.openPopup('product/list');
      }, () => {
        this.toastrService.error('Error', 'Registration failed');
      });
    } else {
      this.toastrService.warning('Warning', 'Fill in the required fields marked with an asterisk');
    }
  }
}
