import {Injectable} from '@angular/core';
import {BaseService} from '../../shared/services/base/base.service';
import {Observable} from 'rxjs';
import {ProductModel} from '../../model/product.model';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends BaseService {

  resourceUrl = 'converter-service/api/catalog';

  getPath(): string {
    return this.resourceUrl;
  }

  getHttpParams(): HttpParams {
    return undefined;
  }

  getAllProduct(): Observable<Array<ProductModel>> {
    return this.get<Array<ProductModel>>();
  }

  createProduct(productModel: ProductModel): Observable<any> {
    return this.post<any>(productModel);
  }

  updateProduct(productModel: ProductModel): Observable<any> {
    return this.put<any>('', productModel);
  }

  findByProduct(id: number): Observable<ProductModel> {
    return this.get('' + id);
  }
}
