import {Component, OnInit} from '@angular/core';
import {ProductService} from '../product.service';
import {ProductModel} from '../../../model/product.model';
import {Router} from '@angular/router';

@Component({
  selector: 'kg-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  allProducts = new Array<ProductModel>();

  constructor(
    private productService: ProductService,
    private router: Router) {
  }

  ngOnInit() {
    this.productService.getAllProduct().subscribe(result => {
      this.allProducts = result;
    });
  }

  addProduct() {
    this.router.navigate(['product/add']);
  }

  editProduct(id: number) {
    this.router.navigate([`product/edit/${id}`]);
  }

}
