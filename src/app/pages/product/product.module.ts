import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductListComponent} from './product-list/product-list.component';
import {ProductEditComponent} from './product-edit/product-edit.component';
import {ProductAddComponent} from './product-add/product-add.component';
import {SharedNgZorroModule} from '../../shared';
import {LayoutModule} from '../../layout/layout.module';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {path: 'list', component: ProductListComponent},
  {path: 'add', component: ProductAddComponent},
  {path: 'edit/:id', component: ProductEditComponent}
];

@NgModule({
  declarations: [ProductListComponent, ProductEditComponent, ProductAddComponent],
  imports: [
    CommonModule,
    LayoutModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    SharedNgZorroModule
  ]
})
export class ProductModule {
}
