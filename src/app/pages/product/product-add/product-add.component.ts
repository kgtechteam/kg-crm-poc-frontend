import {Component, OnInit} from '@angular/core';
import {ProductService} from '../product.service';
import {Router} from '@angular/router';
import {NzNotificationService} from 'ng-zorro-antd';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {ProductModel} from '../../../model/product.model';
import {PopupService} from '../../../shared/services/popup.service';

@Component({
  selector: 'kg-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss']
})
export class ProductAddComponent implements OnInit {

  productModel = new ProductModel();

  productFormGroup = this.formBuilder.group({
    name: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required),
    currency: new FormControl('', Validators.required),
    stock: new FormControl('', Validators.required),
  }, {updateOn: 'blur'});

  constructor(private productService: ProductService,
              private router: Router,
              private toastrService: NzNotificationService,
              private formBuilder: FormBuilder,
              private popupService: PopupService) {
  }

  ngOnInit() {
  }

  addProduct() {
    this.productModel.name = this.productFormGroup.get('name').value;
    this.productModel.price = Number(this.productFormGroup.get('price').value);
    this.productModel.currency = this.productFormGroup.get('currency').value;
    this.productModel.stock = Number(this.productFormGroup.get('stock').value);
    this.productModel.enable = true;
    if (!this.productFormGroup.invalid) {
      this.productService.createProduct(this.productModel).subscribe(() => {
        this.popupService.openPopup('product/list');
      }, () => {
        this.toastrService.error('Error', 'Registration failed');
      });
    } else {
      this.toastrService.warning('Warning', 'Fill in the required fields marked with an asterisk');
    }
  }

}
