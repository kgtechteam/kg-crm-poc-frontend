import {BaseService} from '../../shared/services/base/base.service';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AssetService extends BaseService {
  private resourceUrl = 'asset-service/api/asset';

  getPath(): string {
    return this.resourceUrl;
  }

  getHttpParams(): HttpParams {
    return undefined;
  }

  getDetailById(id: string): Observable<any> {
    return this.get<any>('order/' + id);
  }
}
