import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {OrderService} from '../order.service';
import {OrderModel} from '../../../model/order.model';
import {AssetService} from '../asset.service';
import {NzModalService} from 'ng-zorro-antd';

@Component({
  selector: 'kg-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
  orders: OrderModel[] = [];
  page = 1;
  size = 10;
  total = 0;
  loading = false;


  constructor(
    private orderService: OrderService,
    private assetService: AssetService,
    private router: Router,
    private modalSerive: NzModalService
  ) {
  }

  searchData(reset: boolean = false): void {
    if (reset) {
      this.page = 1;
    }
    this.loading = true;
    this.orderService.page = this.page;
    this.orderService.size = this.size;
    this.orderService.getAllOrder().subscribe(orders => {
      this.loading = false;
      this.total = orders.totalElements;
      this.orders = orders.content;
    });
  }

  ngOnInit() {
    this.searchData();
  }

  addOrder() {
    this.router.navigate(['order/add']);
  }

  detail(id: string) {
    this.assetService.getDetailById(id).subscribe(detail => {
      this.modalSerive.create({
        nzTitle: 'Ip Detail',
        nzContent: detail.ip,
        nzOkText: 'Ok',
        nzCancelText: 'Cancel',
        nzOnOk: () => this.modalSerive.closeAll()
      })
      ;
    });
  }

  getFormattedDate(createDate: string) {
    if (createDate) {
      return createDate.split('T')[0];
    }
  }
}
