import {Component, OnInit} from '@angular/core';
import {OrderService} from '../order.service';
import {Router} from '@angular/router';
import {NzNotificationService} from 'ng-zorro-antd';
import {CustomerModel} from '../../../model/customer.model';
import {CustomerService} from '../../customer/customer.service';
import {ProductService} from '../../product/product.service';
import {ProductModel} from '../../../model/product.model';
import {CreateOrderModel} from '../../../model/create-order.model';
import {PopupService} from '../../../shared/services/popup.service';

@Component({
  selector: 'kg-customer-edit',
  templateUrl: './order-add.component.html',
  styleUrls: ['./order-add.component.scss']
})
export class OrderAddComponent implements OnInit {

  productModel = new Array<ProductModel>();
  customerModel = new Array<CustomerModel>();

  createOrderModel = new CreateOrderModel();

  productId: number;
  tckn: number;

  constructor(private customerService: CustomerService,
              private toastrService: NzNotificationService,
              private router: Router,
              private productService: ProductService,
              private orderService: OrderService,
              private popupService: PopupService
  ) {
  }

  ngOnInit() {
    this.customerService.getAllCustomer().subscribe(result => {
      this.customerModel = result;
    });

    this.productService.getAllProduct().subscribe(result => {
      this.productModel = result;
    });
  }

  selectedProductChange(event) {
    this.createOrderModel.productId = null;
    const product = this.productModel.find(p => p.name === event);
    this.createOrderModel.productId = Number(product.id);
  }

  selectedCustomerChange(event) {
    this.createOrderModel.tckn = null;
    const customer = this.customerModel.find(c => c.name === event);
    this.createOrderModel.tckn = customer.tckn;
  }

  addOrder() {
    if (this.productId && this.tckn) {
      this.orderService.createOrder(this.createOrderModel).subscribe(() => {
        this.popupService.openPopup('order/list');
      }, () => {
        this.toastrService.error('Error', 'Added failed');
      });
    } else {
      this.toastrService.warning('Warning', 'Fill in the required fields marked with an asterisk');
    }
  }

}
