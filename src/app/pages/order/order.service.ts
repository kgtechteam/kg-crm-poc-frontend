import {BaseService} from '../../shared/services/base/base.service';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {OrderModel, PaginationModel} from '../../model/order.model';
import {CreateOrderModel} from '../../model/create-order.model';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrderService extends BaseService {
  private resourceUrl = 'order-service/api/order';

  page = 1;
  size = 10;

  getPath(): string {
    return this.resourceUrl;
  }

  getHttpParams(): HttpParams {
    return new HttpParams()
      .append('page', `${this.page - 1}`)
      .append('size', `${this.size}`);
  }

  getAllOrder(): Observable<PaginationModel<Array<OrderModel>>> {
    return this.get<PaginationModel<Array<OrderModel>>>();
  }

  createOrder(request: CreateOrderModel): Observable<string> {
    return this.post<string>(request);
  }
}
