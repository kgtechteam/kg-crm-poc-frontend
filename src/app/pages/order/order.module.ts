import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LayoutModule} from '../../layout/layout.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedNgZorroModule} from '../../shared';
import {OrderListComponent} from './order-list/order-list.component';
import {OrderAddComponent} from './order-add/order-add.component';

const routes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {path: 'list', component: OrderListComponent},
  {path: 'add', component: OrderAddComponent},
];

@NgModule({
  declarations: [OrderAddComponent, OrderListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LayoutModule,
    ReactiveFormsModule,
    SharedNgZorroModule,
    FormsModule,
  ]
})

export class OrderModule {

}
