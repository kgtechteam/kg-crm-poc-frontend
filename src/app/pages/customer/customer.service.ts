import {BaseService} from '../../shared/services/base/base.service';
import {Observable} from 'rxjs';
import {CustomerModel} from '../../model/customer.model';
import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomerService extends BaseService {
  private resourceUrl = 'converter-service/api/customer';

  getPath(): string {
    return this.resourceUrl;
  }

  getHttpParams(): HttpParams {
    return undefined;
  }

  createCustomerRequest(body: any): Observable<CustomerModel> {
    return this.post<CustomerModel>(body);
  }

  getAllCustomer(): Observable<Array<CustomerModel>> {
    return this.get<Array<CustomerModel>>();
  }

  getCustomerOfTckn(tckn: string): Observable<CustomerModel> {
    return this.post<CustomerModel>('', tckn);
  }

  updateCustomer(customerModel: CustomerModel): Observable<any> {
    return this.put<any>('', customerModel);
  }
}
