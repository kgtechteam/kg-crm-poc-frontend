import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LayoutModule} from '../../layout/layout.module';
import {CustomerListComponent} from './customer-list/customer-list.component';
import {CustomerAddComponent} from './customer-add/customer-add.component';
import {CustomerEditComponent} from './customer-edit/customer-edit.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedNgZorroModule} from '../../shared';

const routes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {path: 'list', component: CustomerListComponent},
  {path: 'add', component: CustomerAddComponent},
  {path: 'edit/:id', component: CustomerEditComponent}
];

@NgModule({
  declarations: [CustomerListComponent, CustomerAddComponent, CustomerEditComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LayoutModule,
    ReactiveFormsModule,
    SharedNgZorroModule,
  ]
})

export class CustomerModule {

}
