import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {CustomerModel} from '../../../model/customer.model';
import {CustomerService} from '../customer.service';
import {NzNotificationService} from 'ng-zorro-antd';
import {AddressService} from '../../address/address.service';
import {AddressModel} from '../../../model/address.model';
import {PopupService} from '../../../shared/services/popup.service';

@Component({
  selector: 'kg-customer-add',
  templateUrl: './customer-add.component.html',
  styleUrls: ['./customer-add.component.scss']
})
export class CustomerAddComponent implements OnInit {

  customerModel = new CustomerModel();
  addresses = new Array<AddressModel>();
  selectedAddressModel = new AddressModel();

  customerFormGroup = this.formBuilder.group({
    tc: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    surname: new FormControl('', Validators.required),
    birthYear: new FormControl('', Validators.required),
    mail: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
  }, {updateOn: 'blur'});

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private customerService: CustomerService,
              private toastrService: NzNotificationService,
              private addressSerivce: AddressService,
              private popupService: PopupService) {
  }

  ngOnInit() {
    this.addressSerivce.getAllAddress().subscribe(result => {
      this.addresses = result;
    });
  }

  addCustomer() {

    const {
      tc,
      name,
      surname,
      birthYear,
      mail,
    } = this.customerFormGroup.value;
    this.customerModel.tckn = Number(tc);
    this.customerModel.name = name;
    this.customerModel.surname = surname;
    this.customerModel.birthYear = Number(birthYear);
    this.customerModel.mail = mail;
    this.customerModel.address = this.selectedAddressModel;

    if (!this.customerFormGroup.invalid) {
      this.customerService.createCustomerRequest(this.customerModel).subscribe(() => {
        this.popupService.openPopup('customer/list');
      }, () => {
        this.toastrService.error('Error', 'Registration failed');
      });
    } else {
      this.toastrService.warning('Warning', 'Fill in the required fields marked with an asterisk');
    }
  }

  selectedCountryChange(event) {
    this.selectedAddressModel = this.addressSerivce.findAddressBySplittedText(this.addresses, event);
  }
}
