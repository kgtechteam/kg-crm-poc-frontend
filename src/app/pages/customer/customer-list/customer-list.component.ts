import {Component, OnInit} from '@angular/core';
import {CustomerModel} from '../../../model/customer.model';
import {Router} from '@angular/router';
import {CustomerService} from '../customer.service';

@Component({
  selector: 'kg-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {
  customers = new Array<CustomerModel>();

  constructor(
    private customerService: CustomerService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.customerService.getAllCustomer().subscribe(customers => {
      this.customers = customers;
    });
  }

  editCustomer(tckn: number) {
    this.router.navigate([`customer/edit/${tckn}`]);
  }

  addCustomer() {
    this.router.navigate(['customer/add']);
  }


}
