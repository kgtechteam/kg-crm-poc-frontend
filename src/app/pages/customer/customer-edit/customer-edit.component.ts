import {Component, OnInit} from '@angular/core';
import {CustomerService} from '../customer.service';
import {ActivatedRoute} from '@angular/router';
import {NzNotificationService} from 'ng-zorro-antd';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {AddressService} from '../../address/address.service';
import {CustomerModel} from '../../../model/customer.model';
import {AddressModel} from '../../../model/address.model';
import {PopupService} from '../../../shared/services/popup.service';

@Component({
  selector: 'kg-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.scss']
})
export class CustomerEditComponent implements OnInit {

  customerUrlName: string;
  editCustomerInformationModel = new CustomerModel();
  selectedAddressModel = new AddressModel();
  addresses = new Array<AddressModel>();
  updateCustomerInformationModel = new CustomerModel();

  customerFormGroup = this.formBuilder.group({
    tc: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    surname: new FormControl('', Validators.required),
    birthYear: new FormControl('', Validators.required),
    mail: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
  }, {updateOn: 'blur'});

  constructor(private customerService: CustomerService,
              private route: ActivatedRoute,
              private toastrService: NzNotificationService,
              private formBuilder: FormBuilder,
              private addressSerivce: AddressService,
              private popupService: PopupService
  ) {
    this.customerUrlName = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.customerService.getCustomerOfTckn(this.customerUrlName).subscribe(result => {
      this.editCustomerInformationModel = result;
      this.fillingInCustomerInformationToBeEditedInTheForm();
    });
    this.addressSerivce.getAllAddress().subscribe(result => {
      this.addresses = result;
    });
  }

  fillingInCustomerInformationToBeEditedInTheForm() {
    this.customerFormGroup.patchValue({
      tc: this.customerUrlName,
      name: this.editCustomerInformationModel.name,
      surname: this.editCustomerInformationModel.surname,
      birthYear: this.editCustomerInformationModel.birthYear,
      mail: this.editCustomerInformationModel.mail,
      address: this.editCustomerInformationModel.address.country + ' ' +
        this.editCustomerInformationModel.address.city + ' ' +
        this.editCustomerInformationModel.address.street
    });
    this.selectedAddressModel = this.editCustomerInformationModel.address;
  }

  selectedAddressChange(event) {
    this.selectedAddressModel = this.addressSerivce.findAddressBySplittedText(this.addresses, event);
  }

  save() {
    this.updateCustomerInformationModel.tckn = Number(this.customerFormGroup.get('tc').value);
    this.updateCustomerInformationModel.name = this.customerFormGroup.get('name').value;
    this.updateCustomerInformationModel.surname = this.customerFormGroup.get('surname').value;
    this.updateCustomerInformationModel.birthYear = Number(this.customerFormGroup.get('birthYear').value);
    this.updateCustomerInformationModel.mail = this.customerFormGroup.get('mail').value;
    this.updateCustomerInformationModel.address = this.selectedAddressModel;

    if (!this.customerFormGroup.invalid) {
      this.customerService.updateCustomer(this.updateCustomerInformationModel).subscribe(() => {
        this.popupService.openPopup('customer/list');
      }, () => {
        this.toastrService.error('Error', 'Registration failed');
      });
    } else {
      this.toastrService.warning('Warning', 'Fill in the required fields marked with an asterisk');
    }
  }
}
