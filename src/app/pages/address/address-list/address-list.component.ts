import {Component, OnInit} from '@angular/core';
import {AddressService} from '../address.service';
import {AddressModel} from '../../../model/address.model';
import {Router} from '@angular/router';

@Component({
  selector: 'kg-address-list',
  templateUrl: './address-list.component.html',
  styleUrls: ['./address-list.component.scss']
})
export class AddressListComponent implements OnInit {

  address: Array<AddressModel>;

  constructor(
    private addressService: AddressService,
    private router: Router
  ) {
  }


  ngOnInit() {
    this.addressService.getAllAddress().subscribe(address => {
      this.address = address;
    });
  }

  editAddress(addressId) {
    this.router.navigate([`address/edit/${addressId}`]);
  }

  addAddress() {
    this.router.navigate(['address/add']);
  }

}
