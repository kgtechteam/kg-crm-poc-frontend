import {BaseService} from '../../shared/services/base/base.service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AddressModel} from '../../model/address.model';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AddressService extends BaseService {
  private resourceUrl = 'converter-service/api/address';

  getPath(): string {
    return this.resourceUrl;
  }

  getHttpParams(): HttpParams {
    return undefined;
  }

  getAllAddress(): Observable<Array<AddressModel>> {
    return this.get<Array<AddressModel>>();
  }

  createAddress(addressDto): Observable<AddressModel> {
    return this.post<AddressModel>(addressDto);
  }

  getAddressDetails(addressId): Observable<AddressModel> {
    return this.get<AddressModel>(addressId);
  }

  updateAddress(addressDto): Observable<AddressModel> {
    return this.put<AddressModel>('', addressDto);
  }

  findAddressBySplittedText(addresses: AddressModel[], searchText: string) {
    let selectedAddress = null;
    addresses.forEach(addressInfo => {
      const addressText = addressInfo.country + ' ' + addressInfo.city + ' ' + addressInfo.street;
      if (searchText === addressText) {
        selectedAddress = addressInfo;
      }
    });
    return selectedAddress;
  }
}
