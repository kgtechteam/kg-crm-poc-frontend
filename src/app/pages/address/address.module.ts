import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LayoutModule} from '../../layout/layout.module';
import {ReactiveFormsModule} from '@angular/forms';
import {AddressAddComponent} from './address-add/address-add.component';
import {AddressEditComponent} from './address-edit/address-edit.component';
import {AddressListComponent} from './address-list/address-list.component';

const routes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {path: 'add', component: AddressAddComponent},
  {path: 'list', component: AddressListComponent},
  {path: 'edit/:id', component: AddressEditComponent}
];

@NgModule({
  declarations: [AddressAddComponent, AddressEditComponent, AddressListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LayoutModule,
    ReactiveFormsModule,
  ]
})
export class AddressModule {

}
