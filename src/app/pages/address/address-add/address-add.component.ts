import {Component, OnInit} from '@angular/core';
import {AddressService} from '../address.service';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {AddressModel} from '../../../model/address.model';
import {Router} from '@angular/router';
import {NzNotificationService} from 'ng-zorro-antd';
import {PopupService} from '../../../shared/services/popup.service';

@Component({
  selector: 'kg-address-add',
  templateUrl: './address-add.component.html',
  styleUrls: ['./address-add.component.scss']
})
export class AddressAddComponent implements OnInit {

  addressDto: AddressModel;

  constructor(
    private addressService: AddressService,
    private formBuilder: FormBuilder,
    private router: Router,
    private toastrService: NzNotificationService,
    private popupService: PopupService
  ) {
  }

  addressFormGroup = this.formBuilder.group({
    country: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    street: new FormControl('', Validators.required),
  }, {updateOn: 'blur'});

  ngOnInit() {
  }

  addAddress() {
    this.addressDto = new AddressModel();
    const {
      country,
      city,
      street
    } = this.addressFormGroup.value;
    this.addressDto.city = city;
    this.addressDto.country = country;
    this.addressDto.street = street;

    if (this.addressFormGroup.valid) {
      this.addressService.createAddress(this.addressDto).subscribe(() => {
        this.popupService.openPopup('address/list');
      }, () => {
        this.toastrService.error('Error', 'Registration failed');
      });
    } else {
      this.toastrService.warning('Warning', 'Fill in the required fields marked with an asterisk');
    }
  }

}
