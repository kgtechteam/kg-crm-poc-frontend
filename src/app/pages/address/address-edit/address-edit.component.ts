import {Component, OnInit} from '@angular/core';
import {AddressService} from '../address.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {AddressModel} from '../../../model/address.model';
import {NzNotificationService} from 'ng-zorro-antd';
import {PopupService} from '../../../shared/services/popup.service';

@Component({
  selector: 'kg-address-edit',
  templateUrl: './address-edit.component.html',
  styleUrls: ['./address-edit.component.scss']
})
export class AddressEditComponent implements OnInit {

  addressDto: AddressModel;
  addressId: string;

  constructor(
    private addressService: AddressService,
    private router: Router,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private toastrService: NzNotificationService,
    private popupService: PopupService
  ) {
    this.addressId = route.snapshot.paramMap.get('id');
  }

  addressFormGroup = this.formBuilder.group({
    country: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    street: new FormControl('', Validators.required),
    id: new FormControl('', Validators.required)
  }, {updateOn: 'blur'});

  ngOnInit() {

    this.addressService.getAddressDetails(this.addressId).subscribe(address => {
      this.addressDto = address;
      this.addressDtoInfo();
    });
  }

  addressDtoInfo() {
    this.addressFormGroup.patchValue({
      city: this.addressDto.city,
      country: this.addressDto.country,
      street: this.addressDto.street,
      id: +this.addressId,
    });
  }

  saveAddress() {
    if (this.addressFormGroup.valid) {
      this.addressService.updateAddress(this.addressFormGroup.value).subscribe(() => {
        this.popupService.openPopup('address/list');
      }, () => {
        this.toastrService.error('Error', 'Registration failed');
      });
    } else {
      this.toastrService.warning('Warning', 'Fill in the required fields marked with an asterisk');
    }
  }

}
