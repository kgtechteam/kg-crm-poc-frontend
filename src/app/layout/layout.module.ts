import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RouterModule } from '@angular/router';
import { MainComponent } from '../main/main.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HeaderComponent} from './header/header.component';
import {SharedNgZorroModule} from '../shared';

@NgModule({
  declarations: [
    SidebarComponent,
    MainComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedNgZorroModule
  ],
  exports: [
    SidebarComponent,
    MainComponent,
    HeaderComponent
  ],
})
export class LayoutModule {
}
