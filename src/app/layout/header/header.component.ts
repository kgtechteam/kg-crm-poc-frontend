import {Component, OnDestroy} from '@angular/core';


import {faSignOutAlt} from '@fortawesome/free-solid-svg-icons';
import {Subscription} from 'rxjs';

@Component({
  selector: 'kg-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnDestroy {
  isCollapsed = false;
  faSignOutAlt = faSignOutAlt;

  subscriptions: Subscription = new Subscription();

  constructor() {
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  toggleCollapse() {

  }

  signOut() {
  }
}
